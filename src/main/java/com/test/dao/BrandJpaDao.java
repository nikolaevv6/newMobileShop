package com.test.dao;

import com.test.domain.Brand;
import com.test.util.HibernateUtil;
import org.hibernate.Query;
import org.hibernate.Session;

import java.util.List;

/**
 * Created by voldem on 07.11.2015.
 */
public class BrandJpaDao implements BrandDao{

    public void add(Brand brand) {
        Session s = HibernateUtil.getSessionFactory().openSession();
        s.beginTransaction();
        s.save(brand);
        s.getTransaction().commit();
        s.close();
    }

    public List getPhones(String brand) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Query query = session.getNamedQuery("allModelsByApple").setString("name",brand);
        List list =  query.list();
        session.close();
        return list;
    }
}
