package com.test.dao;

import com.test.domain.Brand;
import com.test.domain.Phone;

import java.util.List;

/**
 * Created by voldem on 07.11.2015.
 */
public interface BrandDao {
    public void add(Brand brand);

    public List<Phone> getPhones(String brand);
}
