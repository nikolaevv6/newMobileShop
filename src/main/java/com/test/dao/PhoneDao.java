package com.test.dao;

import com.test.domain.Phone;

import java.math.BigDecimal;

/**
 * Created by voldem on 25.10.2015.
 */
public interface PhoneDao {
    void add (Phone phone);
    void sell (Phone phone,int amount);
    BigDecimal getMinPrice();
}
