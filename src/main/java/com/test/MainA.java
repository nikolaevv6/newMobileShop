package com.test;

import com.test.domain.Brand;
import com.test.domain.Phone;
import com.test.service.BrandService;
import com.test.service.BrandServiceImpl;
import com.test.service.PhoneService;
import com.test.service.PhoneServiceImpl;

import java.math.BigDecimal;

/**
 * Created by voldem on 25.10.2015.
 */
public class MainA {
    public static void main(String[] args) {
        PhoneService phoneService = new PhoneServiceImpl();
        BrandService brandService = new BrandServiceImpl();
        Brand brand1 = new Brand("Samsung");
        Brand brand2 = new Brand("Apple");
        Brand brand3 = new Brand("LG");
        brandService.addBrand(brand1);
        brandService.addBrand(brand2);
        brandService.addBrand(brand3);

        Phone phone1 = new Phone();
        phone1.setName("Samsung S6");
        phone1.setBrand(brand1);
        phone1.setPrice(new BigDecimal(15000d));
        phone1.setAmount(5);
        System.out.println(phone1);
        phoneService.addPhone(phone1);

        Phone phone2 = new Phone();
        phone2.setName("iPhone 6s");
        phone2.setBrand(brand2);
        phone2.setPrice(new BigDecimal(25000d));
        phone2.setAmount(5);
        System.out.println(phone2);
        phoneService.addPhone(phone2);

        Phone phone3 = new Phone();
        phone3.setName("LG 100s");
        phone3.setBrand(brand3);
        phone3.setPrice(new BigDecimal(2500d));
        phone3.setAmount(5);
        System.out.println(phone3);
        phoneService.addPhone(phone3);

        Phone phone4 = new Phone();
        phone4.setName("iPhone 5c");
        phone4.setBrand(brand2);
        phone4.setPrice(new BigDecimal(12000d));
        phone4.setAmount(5);
        System.out.println(phone4);
        phoneService.addPhone(phone4);

        Phone phone5 = new Phone();
        phone5.setName("iPhone 5s");
        phone5.setBrand(brand2);
        phone5.setPrice(new BigDecimal(16000d));
        phone5.setAmount(5);
        System.out.println(phone5);
        phoneService.addPhone(phone5);

        System.out.println("Min price = " + phoneService.getMinPrice());

        phoneService.sellPhone(phone2, 2);
        phoneService.sellPhone(phone4, 4);
        phoneService.sellPhone(phone5, 5);

        System.out.println(brandService.getPhones("Apple"));

    }
}