package com.test.domain;

import javax.persistence.*;

/**
 * Created by voldem on 07.11.2015.
 */

@NamedQuery(name = "allModelsByApple", query = "from Phones p where p.brand = :name")



@Entity(name = "Brands")

public class Brand {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column
    private String name;

    public Brand(String name) { this.name = name; }
    public Brand() { }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String toString (){
        return name;
    }

}
