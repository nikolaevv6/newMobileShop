package com.test.domain;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by voldem on 25.10.2015.
 */

@NamedQuery(name = "getMinPrice", query = "select min(price) from Phones")


@Entity(name = "Phones")
public class Phone {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "Model",length = 100)
    private String name;

    @Column(name = "Brand",length = 100)
    private String brand;

    @Column(name = "Price",length = 100)
    private BigDecimal price;

    @Column(name = "Amount",length = 100)
    private Integer amount;


    @Override
    public String toString() {
        return "\nName=" + name +
                ", brand=" + brand +
                ", price=" + price +
                ", amount=" + amount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand.getName();
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }


}
