package com.test.service;

import com.test.dao.BrandDao;
import com.test.dao.BrandJpaDao;
import com.test.domain.Brand;

import java.util.List;

/**
 * Created by voldem on 07.11.2015.
 */
public class BrandServiceImpl implements BrandService{

    private BrandDao brandDao;

    public BrandServiceImpl() {
        brandDao = new BrandJpaDao();
    }

    public void addBrand(Brand brand) {
        brandDao.add(brand);
    }
    public List getPhones(String brand) {
        return brandDao.getPhones(brand);
    }
}
